# Graphs project
This is the test task for working with graphs

## To use this project
You need to perform standart tasks to use this project
```js
git clone https://gitlab.com/ElenaSkakovskaya/graphs.git
cd graphs
npm install
npm run start
```

## ToDO
1. Необходимо организовать загрузку данных с бэкэнда кусками
2. Представление граф

## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

It aims to be an admin panel for user-password info. It should also be protected under the authorization. 


Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).