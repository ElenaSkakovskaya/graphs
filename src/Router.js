import React from 'react';
import {HashRouter, Route} from 'react-router-dom';
import App from './components/App/App';
import AllEntities from './components/AllEntities/AllEntities';
import SelectedEntities from './components/SelectedEntities/SelectedEntities';
import Graph from './containers/Graph/Graph';

const Router = () => (
            <HashRouter>
                <div>
                    <Route path = "/" component = {App} />
                    <Route path = "/" exact component = {AllEntities} />
                    <Route path = "/selected" exact component = {SelectedEntities} />
                    <Route path = "/graph" component = {Graph} />
                </div>
            </HashRouter>
        );

export default Router;
