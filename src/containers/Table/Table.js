import React from 'react';
import ReactTable from "react-table";
import {addSelected, removeSelected} from '../../actions/index';
import {connect} from 'react-redux'
import './Table.css';
import 'react-table/react-table.css';

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.edges = props.isSelected ? props.edges.filter((item) => (props.selected.includes(item.id))) : props.edges;
        this.state = {
            data: this.edges
        };
        this.search = (event) => {
            if (event.key === 'Enter') {
                const searchKey = new RegExp(event.target.value);
                this.setState({
                    data: event.target.value ? this.edges.filter((item) => (searchKey.exec(Object.entries(item).map((item) => item[1])))) : this.edges
                });
            }
        };
        this.addItem = (event, id) => {
            props.rowClickAdd(id);
            event.target.parentElement.parentElement.classList.add('selected');
        };
        this.removeItem = (event, id) => {
            props.rowClickRemove(id);
            event.target.parentElement.parentElement.classList.add('hidden');
        }
    };

    render() {
        return (
            <div className="Table">
                <input type="search" placeholder="Введите текст поиска и нажмите Enter" onKeyPress={this.search}
                       className="Table__input"/>
                <ReactTable
                    data={this.state.data}
                    columns={this.props.columns}
                    defaultPageSize={5}
                    getTrProps={(state, rowInfo, column, instance) => {
                        return {
                            onClick: (e, handleOriginal) => {
                                console.log("It was in this row:", rowInfo.row.id);
                                const rowClickHandler = this.props.isSelected ? this.removeItem : this.addItem;
                                rowClickHandler(e, rowInfo.row.id);
                                if (handleOriginal) {
                                    handleOriginal();
                                }
                            },
                            style: {
                                background: rowInfo && this.props.selected.includes(rowInfo.row.id) ? "#ddffdd" : null
                            }
                        };
                    }}
                />
            </div>)
    }
}
;


const mapStateToProps = (state) => {
    return {
        edges: state.edges,
        selected: state.selected,
        columns: state.columns.objects
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rowClickAdd: (id) => {
            dispatch(addSelected(id));
        },
        rowClickRemove: (id) => {
            dispatch(removeSelected(id));
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Table);