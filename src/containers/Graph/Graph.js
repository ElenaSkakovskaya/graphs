import React from 'react';
import cytoscape from 'cytoscape';
import {addSelected, removeSelected} from '../../actions/index';
import {connect} from 'react-redux'
import './Graph.css';


class Graph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedEdjes: props.edges.filter((item) => (props.selected.includes(item.id))).map((edje) => ({data: edje})),
            edjes: props.edges.map((edje) => ({data: edje})),
            nodes: ((array) => {
                let nodes = [];
                array.forEach((edje) => {
                    for (let dest of ['source', 'target']) {
                        if (edje[dest] && !nodes.includes(edje[dest])) {
                            nodes.push({ data: { id: edje[dest]} });
                        }
                    }
                });
                return nodes;
            })(props.edges),
        };
    }

    componentDidMount() {
        const scriptText = document.createTextNode(`cytoscape(
            {
                container: document.getElementById('cy'),
                elements: ${JSON.stringify([].concat(this.state.nodes).concat(this.state.edjes))},
                style: [
                {
                    selector: 'node',
                    style: {
                        shape: 'ellipse',
                        'background-color': '#ff5555',
                        label: 'data(id)'
                    },
                    layout: {
                        name: 'grid'
                    }
                }]
            }
        )`);

        const script = document.createElement("script");
        script.appendChild(scriptText);

        document.body.appendChild(script);
    }

    render() {
        return (
            <div className="container-fluid Graph">
                <div id="cy" className="Graph__cy"/>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        edges: state.edges,
        selected: state.selected,
        columns: state.columns.objects
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rowClickAdd: (id) => {
            dispatch(addSelected(id));
        },
        rowClickRemove: (id) => {
            dispatch(removeSelected(id));
        }
    }
};

export {cytoscape};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Graph);
