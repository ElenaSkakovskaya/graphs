export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const FILL_COLUMNS = 'FILL_COLUMNS';


export const addSelected = id => ({
    type: ADD_ITEM,
    id
});

export const removeSelected = id => ({
    type: REMOVE_ITEM,
    id
});

export const fillColumns = edges => ({
    type: FILL_COLUMNS,
    edges
});