import {FILL_COLUMNS} from '../actions';

const columns = (state = {list: [], objects: []}, action) => {
    switch (action.type) {
        case FILL_COLUMNS:
            let columnList = state.list.slice();
            let columnObjects = state.objects.slice();
            action.edges.forEach((entity) => {
                Object.entries(entity).forEach((keyValue) => {
                    const column = keyValue[0];
                    if (!columnList.includes(column)){
                        columnList.push(column);
                        columnObjects.push({'Header': column, 'accessor': column});
                    }
                });
            });
            return {list: columnList, objects: columnObjects};
        default:
            return state;
    }
};

export default columns;
