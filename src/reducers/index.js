import { combineReducers } from 'redux';
import {edges, selected} from './edges';
import columns from './columns';

export default combineReducers({
    edges,
    selected,
    columns
});
