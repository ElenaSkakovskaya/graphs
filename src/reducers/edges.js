import {ADD_ITEM, REMOVE_ITEM} from '../actions';

const data = {
    "subgraph": {
        "edges": [
            {
                "properties": 1126544,
                "label": "TO",
                "source": "1024112",
                "target": "102432",
                "id": "jnym-ly7k-iz9-271c"
            }, {
                "properties": 1245448,
                "label": "TO",
                "source": "651352",
                "target": "594096",
                "id": "b8jv-dyl4-iz9-cqeo"
            },
            {
                "properties": 2924744,
                "label": "HAS_ALIAS",
                "source": "1532072",
                "target": "1392672",
                "id": "uwid-wu5k-jrp-tulc"
            }, {
                "properties": 389304,
                "label": "HAS_ALIAS",
                "source": "364560",
                "target": "24744",
                "id": "8feq-7tao-jrp-j3c"
            }, {
                "properties": 852208,
                "label": "TO",
                "source": "770160",
                "target": "82048",
                "id": "e1ou-gi9c-iz9-1rb4"
            },
            {
                "properties": 6815808,
                "label": "FROM",
                "source": "102432",
                "target": "6713376",
                "id": "4ao04-271c-i6t-3zw2o"
            }, {"properties": 3633352, "label": "Email", "summary": "Email 3633352", "id": "3633352"},
            {
                "properties": 1024112,
                "label": "TO",
                "source": "553072",
                "target": "471040",
                "id": "9r7y-bur4-iz9-a3gg"
            },
            {
                "properties": 196656,
                "label": "FROM",
                "source": "102432",
                "target": "94224",
                "id": "1kw4-271c-i6t-20pc"
            }
        ]
    },
    "output": ""
};
const initialState = data.subgraph.edges;
const edges = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
const selected = (state = [], action) => {
    switch (action.type) {
        case ADD_ITEM:
            let newStateA = state.slice();
            if (!newStateA.includes(action.id)){
                newStateA.push(action.id);
            }
            return newStateA;
        case REMOVE_ITEM:
            let newState = state.slice();
            for (let i = newState.length-1; i >= 0; i--){
                if (newState[i] == action.id){
                    newState.splice(i, 1);
                }
            }
            return newState;
        default:
            return state;
    }
};

export {edges, selected} ;
