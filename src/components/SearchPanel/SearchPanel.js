import React from 'react';
import './SearchPanel.css';

const SearchPanel = (props) => (
    <div className="SearchPanel">
        <input type="search"></input>
        <button type="submit">Поиск</button>
    </div>
);

export default SearchPanel;