import React from 'react';
import './SelectedEntities.css';
import TableView from '../TableView/TableView';

const SelectedEntities = (props) =>  (
    <div className="container-fluid">
        <TableView header="Selected entities" isSelected = {true} />
    </div>
);

export default SelectedEntities;
