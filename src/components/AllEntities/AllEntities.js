import React from 'react';
import './AllEntities.css';
import TableView from '../TableView/TableView';

const AllEntities = (props) =>  (
    <div className="container-fluid">
        <TableView header="All entities" isSelected = {false} />
    </div>
);

export default AllEntities;
