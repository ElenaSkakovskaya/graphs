import React from 'react';
import {Link} from 'react-router-dom';
import './App.css';

const App = (props) => (
    <div className="App container">
        <div className="container-fluid App__header">
            <nav className="navbar-nav App__nav navbar-right">
                <ul className="nav navbar-nav">
                    <li>
                        <Link to="/">
                            <span className="glyphicon glyphicon-th-list" />&nbsp;All
                        </Link>
                    </li>
                    <li>
                        <Link to="/selected">
                            <span className="glyphicon glyphicon-folder-open" />&nbsp;&nbsp;Selected
                        </Link>
                    </li>
                    <li>
                        <Link to="/graph">
                            <span className="glyphicon glyphicon-asterisk" />&nbsp;Graph
                        </Link>
                    </li>
                </ul>
            </nav>
        </div>
        {props.children}
    </div>
);

export default App;
