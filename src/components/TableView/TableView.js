import React from 'react';
import Table from '../../containers/Table/Table';
import './TableView.css';

const TableView = (props) => {
    return (
        <div className="TableView">
            <h2>{props.header}</h2>
            <Table isSelected={props.isSelected} />
        </div>
    )
};

export default TableView;